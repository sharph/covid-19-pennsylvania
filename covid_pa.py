#!/usr/bin/env python3

import requests
import re
import datetime
import csv
from pprint import pprint

CASES_URLS = [
    'https://www.health.pa.gov/topics/disease/coronavirus/Pages/Cases.aspx',
    'https://www.health.pa.gov/topics/disease/coronavirus/Pages/Archives.aspx',
    'https://www.health.pa.gov/topics/disease/coronavirus/Pages/April-Archive.aspx',
    'https://www.health.pa.gov/topics/disease/coronavirus/Pages/March-Archive.aspx',
]


day_split = re.compile(r'([A-Z][a-z]+? [0-9]+?, 2020)', flags=re.DOTALL)
date_re = re.compile(r'>(| +?)([A-Z][a-z]+? [0-9]+?, 2020)(| +?)<')
tables_re = re.compile(r'(\<table.+?\</table\>)', flags=re.IGNORECASE | re.DOTALL)
table_id_re = re.compile(r'County.*(Positives|Cases).*Deaths', flags=re.IGNORECASE | re.DOTALL)
rows_re = re.compile(r'(\<tr.+?\/tr\>)', flags=re.IGNORECASE | re.DOTALL)
cols_re = re.compile(r'\<td.+?\>(.+?)\<\/td\>', flags=re.IGNORECASE | re.DOTALL)
li_re = re.compile(r'\<li\>([A-Za-z]+? \([0-9]+?\))', flags=re.IGNORECASE | re.DOTALL)

text_re = re.compile(r'([A-Za-z0-9]*)')


def cleanup_county(text):
    if 'span' in text:
        text = text.split('>')[1].split('<')[0]
    if text.startswith('\u200b'):
        text = text[1:]
    if '<br>' in text:
        text = text.split('<br>')[0]
    if '&' in text:
        text = text.split('&')[0]
    text = text.strip()
    text = text[0].upper() + text[1:].lower()
    if text == 'Mckean':
        text = 'McKean'
    return text


def cleanup_number(text):
    text = text.lstrip('(').rstrip(')')
    if text.startswith('\u200b'):
        text = text[1:]
    if '<br>' in text:
        text = text.split('<br>')[0]
    return int(text) if text.isnumeric() else 0


def parse_archive(src):
    days = {}
    txt = day_split.split(src)
    days = dict(zip(txt[1::2], txt[2::2]))

    parsed = {}
    for day in days.keys():
        decoded_day = datetime.datetime.strptime(day, '%B %d, %Y').date()
        parsed[decoded_day] = {}
        if decoded_day < datetime.date(2020, 3, 16):
            for county, num in map(str.split, li_re.findall(days[day])):

                parsed[decoded_day][cleanup_county(county)] = {
                    'Total Cases': cleanup_number(num),
                    'Deaths': 0
                }
            continue
        for table in tables_re.findall(days[day]):
            if not table_id_re.search(table) and decoded_day > datetime.date(2020, 3, 19):
                continue
            if 'Negative' in table and decoded_day < datetime.date(2020, 3, 20):
                continue
            if 'Facility' in table:
                continue
            for row in rows_re.findall(table):
                if table_id_re.search(row):
                    continue
                decoded_row = [col for col in cols_re.findall(row)]
                if decoded_row[0] == 'Pennsylvania':
                    continue
                if len(decoded_row) == 4:
                    parsed[decoded_day][cleanup_county(decoded_row[0])] = {
                        'Total Cases': cleanup_number(decoded_row[1]),
                        'Negatives': cleanup_number(decoded_row[2]),
                        'Deaths': cleanup_number(decoded_row[3])
                    }
                elif len(decoded_row) == 3:
                    parsed[decoded_day][cleanup_county(decoded_row[0])] = {
                        'Total Cases': cleanup_number(decoded_row[1]),
                        'Deaths': cleanup_number(decoded_row[2])
                    }
                elif len(decoded_row) == 2:
                    parsed[decoded_day][cleanup_county(decoded_row[0])] = {
                        'Total Cases': cleanup_number(decoded_row[1]),
                        'Deaths': 0
                    }
    return parsed


def get_all_stats():
    covid_stats = {}

    for url in CASES_URLS:
        res = requests.get(url)
        src = res.text
        covid_stats.update(parse_archive(src))

    return covid_stats


if __name__ == '__main__':
    stats = list(get_all_stats().items())
    stats.sort(key=lambda x: x[0])  # sort on date
    pprint(stats)
    counties = list(stats[-1][1].keys())
    for filename, field in (('cases.csv', 'Total Cases'), ('deaths.csv', 'Deaths')):
        with open(filename, 'w',  newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['Date'] + counties)
            writer.writeheader()
            for day, data in stats:
                row = {'Date': str(day)}
                row.update({county: day_data[field] for county, day_data in data.items()})
                writer.writerow(row)


